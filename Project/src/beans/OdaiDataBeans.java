package beans;

import java.io.Serializable;
import java.util.Date;

public class OdaiDataBeans implements Serializable {

	/**
	 * お題データ
	 */

	private String odai;
	private String toukousya;
	private Date createDate;

	public String getOdai() {
		return odai;
	}

	public void setOdai(String odai) {
		this.odai = odai;
	}

	public String getToukousya() {
		return toukousya;
	}

	public void setToukousya(String toukousya) {
		this.toukousya = toukousya;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
			this.createDate = createDate;
		}
}
