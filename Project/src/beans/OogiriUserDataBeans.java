package beans;

import java.io.Serializable;
import java.util.Date;

/**
 * ユーザーデータ
 */
public class OogiriUserDataBeans implements Serializable {
	private int id;
	private String name;
	private String syougou;
	private String password;
	private Date createDate;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSyougou() {
		return syougou;
	}

	public void setSyougou(String syougou) {
		this.syougou = syougou;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

}