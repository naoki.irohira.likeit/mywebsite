package beans;

import java.io.Serializable;
import java.util.Date;

public class CommentDataBeans implements Serializable {

	/**
	 * コメントデータ
	 */

	private String comments;
	private String commenter;
	private Date createDate;

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getCommenter() {
		return commenter;
	}

	public void setCommenter(String commenter) {
		this.commenter = commenter;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

}