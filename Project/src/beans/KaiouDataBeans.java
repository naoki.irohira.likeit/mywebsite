package beans;

import java.io.Serializable;
import java.util.Date;

public class KaiouDataBeans implements Serializable {

	/**
	 * 回答データ
	 */

	private String kaitou;
	private String kaitousya;
	private Date createDate;

	public String getKaitou() {
		return kaitou;
	}

	public void setKaitou(String kaitou) {
		this.kaitou = kaitou;
	}

	public String getKaitousya() {
		return kaitousya;
	}

	public void setKaitousya(String kaitousya) {
		this.kaitousya = kaitousya;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

}