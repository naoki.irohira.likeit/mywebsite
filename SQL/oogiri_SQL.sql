﻿use oogiri;

create table oogiri_user(
id int PRIMARY KEY AUTO_INCREMENT,
  name varchar(256) not null,
  password varchar(256) not null,
  syougou varchar(256) not null,
  star int,
  create_date date not NULL
);


use oogiri;

create table odai(
  odai varchar(256) not NULL,
  toukousya varchar(256) not NULL,
  create_date date not NULL,
);

use oogiri;

create table kaitou(
  kaitou varchar(256) not NULL,
  kaitousya varchar(256) not NULL,
  create_date date not NULL,
   star int
);

use oogiri;

create table comments(
 comments varchar(256) not NULL,
  commenter varchar(256) not NULL,
  create_date date not NULL
);

